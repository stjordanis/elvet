Installation
============

Elvet is available at pypi, so it can be installed by running::

  $ pip install elvet


Python >=3.6 is required. Elvet is based on `tensorflow <https://www.tensorflow.org/>`_ to work, so it will be installed by ``pip``.

In order to use Elvet's plotting functionality, `matplotlib <https://www.tensorflow.org/>`_ is also needed, and has to be installed independently.
