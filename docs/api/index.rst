Description of all functions and classes
========================================

.. toctree::
   :maxdepth: 1

   elvet
   elvet.math
   elvet.utils

.. autosummary::
   elvet.BC
   elvet.box
   elvet.cut_domain
   elvet.ellipsoid
   elvet.nn
   elvet.unstack
   elvet.solver
   elvet.minimizer
   elvet.fitter
   elvet.Solver
   elvet.Minimizer
   elvet.math.derivative_stack
   elvet.math.derivative
   elvet.math.integral
   elvet.math.integration_methods
   elvet.math.divergence
   elvet.math.curl
   elvet.math.laplacian
   elvet.math.dalembertian
   elvet.math.manifold_divergence
   elvet.math.laplace_beltrami
   elvet.math.metrics
   elvet.math.diagonals
