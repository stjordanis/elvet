The elvet.math subpackage
=========================

.. autosummary::
   elvet.math.derivative_stack
   elvet.math.derivative
   elvet.math.integral
   elvet.math.integration_methods
   elvet.math.divergence
   elvet.math.curl
   elvet.math.laplacian
   elvet.math.dalembertian
   elvet.math.manifold_divergence
   elvet.math.laplace_beltrami
   elvet.math.metrics
   elvet.math.diagonals

.. automodule:: elvet.math
    :members:
    :undoc-members:
    :show-inheritance:
