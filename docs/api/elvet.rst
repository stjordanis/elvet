The elvet package
-----------------

.. autosummary::
   elvet.BC
   elvet.box
   elvet.cut_domain
   elvet.ellipsoid
   elvet.nn
   elvet.unstack
   elvet.solver
   elvet.minimizer
   elvet.fitter
   elvet.Solver
   elvet.Minimizer

.. automodule:: elvet
    :members:
    :undoc-members:
    :show-inheritance:
