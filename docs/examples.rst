========
Examples
========

The following examples can be run online at Google Colaboratory:

Differential equations
----------------------

- A `simple ODE <https://colab.research.google.com/github/jccriado/elvet_examples/blob/main/differential_equations/1_simple_ode.ipynb>`_.

- The quantum harmonic oscillator `Schrödinger equation <https://colab.research.google.com/github/jccriado/elvet_examples/blob/main/differential_equations/2_quantum_harmonic_oscillator.ipynb>`_.

- The Lotka-Volterra `system of equations <https://colab.research.google.com/github/jccriado/elvet_examples/blob/main/differential_equations/3_lotka_volterra.ipynb>`_.

- Another system of `coupled ODEs <https://colab.research.google.com/github/jccriado/elvet_examples/blob/main/differential_equations/4_coupled_ode.ipynb>`_.

- A `PDE <https://colab.research.google.com/github/jccriado/elvet_examples/blob/main/differential_equations/5_simple_pde.ipynb>`_.

- An `illustration <https://colab.research.google.com/github/jccriado/elvet_examples/blob/main/differential_equations/5_simple_pde.ipynb>`_ of how batch mode works.

Variational problems (functional minimization)
----------------------------------------------

- The `catenary <https://colab.research.google.com/github/jccriado/elvet_examples/blob/main/variational_problems/catenary.ipynb>`_, the shape of a hanging chain.

- The `brachistochrone <https://colab.research.google.com/github/jccriado/elvet_examples/blob/main/variational_problems/brachistochrone.ipynb>`_, the curve of shortest descend.

- The `geodesics <https://colab.research.google.com/github/jccriado/elvet_examples/blob/main/variational_problems/hyperbolic_geodesic.ipynb>`_ of the hyperbolic plane.

Others
------

- `Generating non-trivial 2D domains <https://colab.research.google.com/github/jccriado/elvet_examples/blob/main/others/domains.ipynb>`_.

- `Fitting a curve <https://colab.research.google.com/github/jccriado/elvet_examples/blob/main/others/fitting_breit_wigner_sum.ipynb>`_, a sum of Breit-Wigner distributions.

  
